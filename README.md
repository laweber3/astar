**A\* (A-Star) search algorithm**

This is an implementation of the A* (A-Star) search algorithm based on
Dijkstra's algorithm. It is written to Java interfaces so it's easy
to integrate with your existing code. Just write classes which implement
the Graph and GraphNode interfaces and you're all set.

```
Graph {
	Iterator<? extends GraphNode> getNeighborNodes(GraphNode n);
	int getCostToNeighbor(GraphNode n1, GraphNode n2);
	int estimateCostToEnd(GraphNode startNode, GraphNode endNode);
}

GraphNode {
	String getNodeName();
	boolean equals(Object obj);
	int hashCode();
}
```

The only one that might be a bit tricky to implement is `estimateCostToEnd()`,
which is defined as:

> Known as the heuristic algorithm, estimateCostToEnd() estimates the travel
> cost from a current node to an end node. The heuristic is said
> to be "admissible", and thus guarantees the A* algorithm will
> find the shortest path, if it never over estimates the cost. The
> closer it is to the actual cost the better the performance of the
> path finding algorithm will be.

Once you've implemented the interfaces, the following methods on the PathFinder
class will be available for you to call.

```
List<GraphNode> findShortestPath(GraphNode startNode, GraphNode endNode, Graph graph);
int findLowestPathCost(GraphNode startNode, GraphNode endNode, Graph graph);
Set<GraphNode> findAllNodes(GraphNode startNode, int maxDistance, Graph graph);
```
